# OpenVentilator ST10 Firmware

Firmware for the PB560 ST10 chip


## Hardware

* MCU [ST10F276Z5](https://www.st.com/en/microcontrollers-microprocessors/st10f276z5.html)

## Original Development Platform

* Windows OS (**hard requirement for now**)
* Compiler: Keil XC16x/C16x/ST10 Compilation [Tools](http://www.keil.com/c166/ca166kit.asp)
* PK166 Professional Developer's [Kit](http://www.keil.com/c166/pk166kit.asp)

## Toolchain and Getting started

1. Follow instructions found [here](https://docs.google.com/document/d/1VwWOs-kJtXTly-YLTWak2quROzQP8Es-8u3Libn3Isg/edit)
